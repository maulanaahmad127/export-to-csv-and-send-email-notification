package com.crud.controller;



import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.crud.model.Employee;
import com.crud.service.CsvReportService;
import com.crud.service.EmployeeService;
import com.crud.service.ExcelExporter;
import com.crud.service.ReportService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;


import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;



@Controller
public class ReportController {
    
    @Autowired
    private ReportService service;

	@Autowired
	private EmployeeService employeeService;

    @Autowired
    private HttpServletResponse response;

    @Autowired
    private CsvReportService csvReportService;

   
    @GetMapping("/reports/pdf")
    public void getReport() throws Exception {
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=Employee_listxxx.pdf");
        JasperPrint jasperPrint = service.generateJasperPrint();
        JasperExportManager.exportReportToPdfStream(jasperPrint, response.getOutputStream());
    }

    @GetMapping("/users/export/excel")
    public void getReportExcel() throws Exception {
		response.setContentType("application/octet-stream");
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
        String currentDateTime = dateFormatter.format(new Date());
         
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";
        response.setHeader(headerKey, headerValue);
         
        Iterable<Employee> listUsers = employeeService.findAll();
		List<Employee> users = new ArrayList<Employee>();
		for(Employee e: listUsers) {
			users.add(e);
		}
         
        ExcelExporter excelExporter = new ExcelExporter(users);
         
        excelExporter.export(response);   
        
    }

    @GetMapping("/reports/csv")
    public void getAllEmployeesInCsv() throws IOException {
        response.setContentType("text/csv");
        response.addHeader("Content-Disposition","attachment; filename=\"employees.csv\"");
        csvReportService.writeEmployeesToCsv(response.getWriter());
    }
}
