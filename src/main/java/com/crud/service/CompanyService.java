package com.crud.service;


import java.util.Optional;

import com.crud.model.Company;
import com.crud.repo.CompanyRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CompanyService {

    @Autowired
    private CompanyRepo repo;


    public Iterable<Company> findAll(){
        return repo.findAll();
    }

    public Optional<Company> findById(long id){
       return repo.findById(id);
    }

    public void add(Company company){
        repo.save(company);
    }

    public void update(Company company){
        repo.save(company);
    }

    public void delete(long id){
        repo.deleteById(id);
    }
}
