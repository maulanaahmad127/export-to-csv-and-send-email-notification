package com.crud.service;

import java.io.IOException;
import java.io.Writer;
import java.lang.System.Logger;
import java.util.List;

import com.crud.model.Employee;
import com.crud.repo.EmployeeRepo;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class CsvReportService {
    
    private static final Logger log = getLogger(CsvReportService.class);

    @Autowired
    private EmployeeService employeeService;

   

    private static Logger getLogger(Class<CsvReportService> class1) {
        return null;
    }

    public void writeEmployeesToCsv(Writer writer) {

        Iterable<Employee> employees = employeeService.findAll();
        try (CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT)) {
            for (Employee employee : employees) {
                csvPrinter.printRecord(employee.getId(), employee.getNama(), employee.getPosisi(), employee.getCompanyId(), employee.getCompanyName());
            }
        } catch (IOException e) {
            System.out.println("Error while writing csv" + e.getMessage());
        }
    }
}
