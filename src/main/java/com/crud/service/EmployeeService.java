package com.crud.service;


import java.util.Optional;

import com.crud.model.Employee;
import com.crud.repo.EmployeeRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepo repo;

    @Autowired
    private EmailService emailService;


    public Iterable<Employee> findAll(){
        return repo.findAll();
    }

    public Optional<Employee> findById(int id){
        
       return repo.findById(id);
    }

    public void add(Employee employee){

        emailService.sendEmail("maulahmaddd40@gmail.com", "New Employee Created !!!", "You have added employee " + employee.getNama() + " Posisi " + employee.getPosisi());
        repo.save(employee);
    }

    public void update(Employee employee){
        repo.save(employee);
    }

    public void delete(int id){
        
        repo.deleteById(id);
    }
}
