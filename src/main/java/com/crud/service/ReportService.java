package com.crud.service;

import java.io.InputStream;
import java.sql.Connection;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;


@Service
public class ReportService {
    
    @Autowired
    private DataSource dataSource;

    private Connection getConnection(){
        try{
            return dataSource.getConnection();
        }catch(Exception e){
            System.err.println(e);
            return null;
        }
    };

    public JasperPrint generateJasperPrint() throws Exception{
        InputStream file = new ClassPathResource("reports/EmployeeReport.jasper").getInputStream();
        JasperReport report = (JasperReport) JRLoader.loadObject(file);
        JasperPrint print = JasperFillManager.fillReport(report, null, getConnection());
        return print;
    }

    
    
}
